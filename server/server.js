var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    fs = require('fs'),
    app = express(),
    port = process.env.PORT || 8888;

// use compression
app.use(compression());

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(__dirname + '/logs/access.log', {flags: 'a'});
// setup the logger
app.use(morgan('combined', {stream: accessLogStream}));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/activate', express.static(__dirname + '/../dist'));

app.use('/activate/test/unit', express.static(__dirname + '/../test/unit/report'));

app.use('/jsontest', function(req, res) {
    var testObj = {test: 'test', a: 'a', b: 'b'};
    res.status(200).json(testObj);
});

app.listen(port);
