// Include gulp
var gulp = require('gulp');

// plugins!
 var lib = require('bower-files')(),
    less = require('gulp-less'),
    mocha = require('gulp-mocha'),
    gulpif = require('gulp-if'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    useref = require('gulp-useref'),
    nodemon = require('gulp-nodemon'),
    livereload = require('gulp-livereload'),
    del = require('del');

// Paths
var config = {
        files: {
            js: 'app/js/*.js',
            less: 'app/less/*.less'
        },
        dest: {
            js: 'dist/js',
            less: 'dist/css',
        }
    };

// Less task
gulp.task('less', function() {
    return gulp.src(config.files.less)
        .pipe(less())
        .pipe(gulp.dest('dist/css/'))
        .pipe(gulp.dest('app/css'))
        .pipe(livereload());
});


// Lint Task
gulp.task('lint', function(cb) {
    return gulp.src(config.files.js)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-reporter-jscs'))
        .pipe(jshint.reporter('fail'));
});

// create our scripts
gulp.task('scripts', ['lint'], function() {
    return gulp.src(config.files.js)
        .pipe(gulp.dest('dist/js'))
        .pipe(livereload());
});

// concat our bower libs
gulp.task('bower', ['bower-js', 'bower-css']);

gulp.task('bower-js', function() {
    return gulp.src(lib.ext('js').files)
        .pipe(concat('lib.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('bower-css', function() {
    return gulp.src(lib.ext('css').files)
        .pipe(concat('lib.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('dist/css'));
});

// copy over the html
gulp.task('html', function() {
    return gulp.src('app/*.html')
        .pipe(gulp.dest('dist'))
        .pipe(livereload());
});

// bundle stuff
gulp.task('bundle', ['scripts', 'bower'], function() {
    var assets = useref.assets();

    return gulp.src('app/*.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minify()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});


// test tasks
gulp.task('unit-test', function() {
    return gulp.src('test/unit/*.js', {read: false})
        .pipe(mocha({
            reporter: 'mochawesome',
            reporterOptions: {
                reportDir: 'test/unit/report',
                reportName: 'index'
            }
        }));
});

// Nodemon 
gulp.task('nodemon', ['build'], function() {
    nodemon({
        script: 'server/server.js',
        watch: ['server/*'],
    });
});

// clean up the build 
gulp.task('clean', function(cb) {
    del(['dist/css', 'app/css', 'dist/js'], cb);
});

// Watcher task
gulp.task('watch', ['build'], function() {
    livereload.listen();
    gulp.watch('app/*.html', ['html']);
    gulp.watch('app/js/*.js', ['lint', 'scripts']);
    gulp.watch('app/less/*.less', ['less']);
});

gulp.task('build', ['less', 'lint', 'scripts', 'bower', 'bundle']);

gulp.task('develop', ['build', 'watch', 'nodemon']);

gulp.task('default', ['develop']);
